# AUTO - SPOTIFY #

## Acknowledgment ##

The Go Spotify Web API used in this repo was built by [Zac Bergquist](https://github.com/zmb3/spotify). It offers a bunch of endpoints.

## How to start ##

To use this repo, you will need to set up a [Spotify](https://www.spotify.com/) account. Register this application and get a Cliend ID and a Client Secret, save them in a new .go file as variables. Click [here](https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app) for more about app settings and registering your app.

## How to use ##

```
% git clone https://fabricekwizera@bitbucket.org/fabricekwizera/auto-spotify.git

% cd auto-spotify/


% go run *.go

```
## Sample output ##

```

                        PLEASE LOG IN TO SPOTIFY BY VISITING BELOW LINK IN YOUR BROWSER
----------------------------------------------------------------------------------------------------------------------
https://accounts.spotify.com/authorize?client_id=ed4e254c9050455aa3a3a43747b3f20e&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fcallback&response_type=code&scope=user-read-private+playlist-modify-private+user-read-playback-state&state=abc123
----------------------------------------------------------------------------------------------------------------------

YOU ARE LOGGED IN AS: fabrice

  - PROVIDE THE NAME OF THE ARTIST YOU WANT TO LISTEN TO: jay-z
  - PROVIDE THE NAME OF THE PLAYLIST TO BE CREATED: Jay-Z's Rhymes          
  - PROVIDE THE DESCRIPTION OF THE PLAYLIST: Some good rap music

ALL EXISTING PLAYLISTS FOR THIS USER

  - Relaxing
  - What can I say?
  - 90's Hip-Hop Don't Stop
  - Hot Country


SONGS ADDED TO THE PLAYLIST 
  - Ni**as In Paris is added
  - Empire State Of Mind is added
  - Umbrella is added
  - Crazy In Love (feat. Jay-Z) is added
  - Biking is added
  - Monster is added
  - Drunk in Love (feat. Jay-Z) is added
  - Numb / Encore is added
  - Umbrella is added
  - Run This Town is added
  - No Church In The Wild is added
  - Ni**as In Paris is added
  - Otis is added
  - Holy Grail is added
  - Suit & Tie is added
  - The Story of O.J. is added
  - Numb / Encore is added
  - Big Pimpin' is added
  - Clique is added
  - Hard Knock Life (Ghetto Anthem) is added


```