module bitbucket.org/fabricekwizera/auto-spotify

go 1.13

require (
	github.com/aws/aws-sdk-go v1.29.9 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/zmb3/spotify v0.0.0-20200112163645-71a4c67d18db
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
