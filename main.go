package main

import (
	"fmt"

	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/zmb3/spotify"
)

const redirectURI = "http://localhost:8080/callback"

var (
	auth  = spotify.NewAuthenticator(redirectURI, spotify.ScopeUserReadPrivate, spotify.ScopePlaylistModifyPrivate, spotify.ScopeUserReadPlaybackState)
	ch    = make(chan *spotify.Client)
	state = "abc123"
)

func main() {
	// first start an HTTP server
	auth.SetAuthInfo(client_id, client_secret)
	http.HandleFunc("/callback", completeAuth)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//log.Println("Got request for:", r.URL.String())
	})
	go http.ListenAndServe(":8080", nil)

	url := auth.AuthURL(state)
	fmt.Println()
	fmt.Println("						PLEASE LOG IN TO SPOTIFY BY VISITING BELOW LINK IN YOUR BROWSER")
	fmt.Println("----------------------------------------------------------------------------------------------------------------------")
	fmt.Println(url)
	fmt.Println("----------------------------------------------------------------------------------------------------------------------")

	// wait for auth to complete
	client := <-ch

	// use the client to make calls that require authorization
	user, err := client.CurrentUser()
	FatalLog(err)
	fmt.Println()
	fmt.Println("YOU ARE LOGGED IN AS:", user.ID)
	fmt.Println()
	artist, playlistName, playlistDesc := userInput()
	listPlaylists(client, user.ID)

	// Creation of a playlist in on specified Spotify account

	if len(artist) != 0 {
		if len(playlistName) != 0 || len(playlistDesc) != 0 {
			fmt.Println()
			fmt.Println("BOTH THE NAME AND DESCRIPTION OF THE PLAYLIST SHOULD BE PROVIDED")
		} else {
			playlist, err := client.CreatePlaylistForUser(user.ID, playlistName, playlistDesc, false)
			FatalLog(err)

			// Searching songs by an artist and add them to the recently created playlist
			searchAndAddSongs(artist, client, playlist.ID)
		}
	} else {
		fmt.Println()
		fmt.Println("THE NAME OF THE ARTIST IS MISSING")
	}
}

func completeAuth(w http.ResponseWriter, r *http.Request) {
	tok, err := auth.Token(state, r)
	if err != nil {
		http.Error(w, "Couldn't get token", http.StatusForbidden)
		log.Fatal(err)
	}
	if st := r.FormValue("state"); st != state {
		http.NotFound(w, r)
		log.Fatalf("State mismatch: %s != %s\n", st, state)
	}
	// use the token to get an authenticated client
	client := auth.NewClient(tok)
	fmt.Fprintf(w, "Login Completed!")
	ch <- &client
}
