package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/zmb3/spotify"
)

func FatalLog(err error) {
	if err != nil {
		log.Fatalf("The error: %v", err)
	}
}

// Listing all the users's playlists
func listPlaylists(client *spotify.Client, ID string) {
	allPlaylists, err := client.GetPlaylistsForUser(ID)
	FatalLog(err)
	fmt.Println()
	fmt.Println("ALL EXISTING PLAYLISTS FOR THIS USER")
	fmt.Println()
	if allPlaylists.Playlists != nil {
		for _, playlist := range allPlaylists.Playlists {
			fmt.Printf("  - %s", playlist.Name)
			fmt.Println()
		}
	}
}

// Dealing with input coming in through the command line
func userInput() (string, string, string) {
	fmt.Print("  - PROVIDE THE NAME OF THE ARTIST YOU WANT TO LISTEN TO: ")
	reader := bufio.NewReader(os.Stdin)
	artist, _ := reader.ReadString('\n')
	fmt.Print("  - PROVIDE THE NAME OF THE PLAYLIST TO BE CREATED: ")
	playlistName, _ := reader.ReadString('\n')
	fmt.Print("  - PROVIDE THE DESCRIPTION OF THE PLAYLIST: ")
	playlistDesc, _ := reader.ReadString('\n')
	return strings.TrimSpace(artist), strings.TrimSpace(playlistName), strings.TrimSpace(playlistDesc)
}

// Dealing with making searches and adding songs to the newly created playlist
func searchAndAddSongs(artist string, client *spotify.Client, playlistID spotify.ID) {
	songsLimit := 20
	result, err := client.SearchOpt(artist, spotify.SearchTypeTrack|spotify.SearchTypeAlbum, &spotify.Options{Limit: &songsLimit})
	FatalLog(err)
	fmt.Println()
	fmt.Println()
	if result.Tracks != nil {
		fmt.Println("SONGS ADDED TO THE PLAYLIST ")
		for _, track := range result.Tracks.Tracks {
			client.AddTracksToPlaylist(playlistID, track.ID)
			fmt.Printf("  - %s is added", track.Name)
			fmt.Println()
		}
	}
}
